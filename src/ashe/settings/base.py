import os
from os.path import abspath, join
from django.utils.translation import ugettext_lazy as _
from ashe.utils import loadenv

gettext = lambda s: s

PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_DIR = os.path.dirname(PROJECT_DIR)

location = lambda path: abspath(join(BASE_DIR, path))
loadenv(location("../.env"))

SECRET_KEY = os.environ.get("DJANGO_SECRET_KEY", "secret")

DEBUG = True

ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = [
    # Django
    "django_grapesjs",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",

    # Django-cms
    "cms",
    "menus",
    "treebeard",
    "sekizai",
    "filer",
    "meta",
    "easy_thumbnails",
    "djangocms_text_ckeditor",
    "djangocms_page_meta",

    # Django apps
    "ashe.apps.pages",
]

MIDDLEWARE = [
    "cms.middleware.utils.ApphookReloadMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "cms.middleware.user.CurrentUserMiddleware",
    "cms.middleware.page.CurrentPageMiddleware",
    "cms.middleware.toolbar.ToolbarMiddleware",
    "cms.middleware.language.LanguageCookieMiddleware",
]

ROOT_URLCONF = "ashe.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(PROJECT_DIR, "templates/")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "cms.context_processors.cms_settings",
                "sekizai.context_processors.sekizai",
            ],
        },
    },
]

WSGI_APPLICATION = "ashe.wsgi.application"

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "HOST": os.environ.get("DJANGO_DATABASE_HOST", "database"),
        "NAME": os.environ.get("DJANGO_DATABASE_NAME"),
        "USER": os.environ.get("DJANGO_DATABASE_USER"),
        "PASSWORD": os.environ.get("DJANGO_DATABASE_PASSWORD", "postgres"),
    }
}

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",},
]

# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = "en"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True

SITE_ID = 1

LANGUAGES = [
    ("es", _("spanish")),
    ("en", _("english")),
]

LOCALE_PATHS = [os.path.join(BASE_DIR, "../locale")]

CMS_LANGUAGES = {
    ## Customize this
    "default": {
        "public": True,
        "hide_untranslated": True,
        "redirect_on_fallback": True,
    },
    1: [
        {
            "public": True,
            "code": "es",
            "hide_untranslated": True,
            "name": gettext("es"),
            "redirect_on_fallback": True,
        },
        {
            "public": True,
            "code": "en",
            "hide_untranslated": True,
            "name": gettext("en"),
            "redirect_on_fallback": True,
        },
    ],
}

CMS_PERMISSION = False

CMS_TEMPLATES = (("pages/home.html", "Home"),)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]

STATICFILES_DIRS = [os.path.join(PROJECT_DIR, "static/")]

STATIC_ROOT = os.path.join(BASE_DIR, "../public/static")
STATIC_URL = "/static/"

MEDIA_ROOT = os.path.join(BASE_DIR, "../public/media")
MEDIA_URL = "/media/"

THUMBNAIL_PROCESSORS = (
    "easy_thumbnails.processors.colorspace",
    "easy_thumbnails.processors.autocrop",
    "filer.thumbnail_processors.scale_and_crop_with_subject_location",
    "easy_thumbnails.processors.filters",
    "easy_thumbnails.processors.background",
)

# Assets for grapes.js library
GRAPESJS_CORE_ASSETS = {
    'js': 'https://unpkg.com/grapesjs',
    'css': 'https://unpkg.com/grapesjs/dist/css/grapes.min.css'
}

# A set of plugins with their assets
GRAPESJS_PLUGIN_ASSETS = {
    'grapesjs-blocks-basic': {
        'js': 'https://cdn.jsdelivr.net/npm/grapesjs-blocks-basic@0.1.8/dist/grapesjs-blocks-basic.min.js',
    },
    'grapesjs-preset-webpage': {
        'js': 'https://cdn.jsdelivr.net/npm/grapesjs-preset-webpage@0.1.11/dist/grapesjs-preset-webpage.min.js'
    }
}

GRAPESJS_FORM = 'pages.GrapesJSCreateForm'

GRAPESJS_MODEL = 'pages.GrapesJSJSONModel'
GRAPESJS_CREATE_FORM = 'pages.forms.GrapesJSCreateForm'
GRAPESJS_UPDATE_FORM = 'pages.forms.GrapesJSUpdateForm'
