from django.conf import settings
from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, re_path
from django.views.i18n import JavaScriptCatalog
from django_grapesjs.views.admin import GetTemplate

from ashe.apps.pages.views import (
    GrapesJSTemplateView, GrapesJSDetailView,
    GrapesJSCreateView, GrapesJSDeleteView,
    GrapesJSListView,
    GrapesJSLoadView, GrapesJSUpdateView
)


urlpatterns = [
    path("admin/", admin.site.urls),

    re_path('^template/list/$', GrapesJSListView.as_view(), name='template-list'),
    re_path('^template/create/', GrapesJSCreateView.as_view(), name='template-create'),
    re_path('^template/(?P<pk>\d+)/?$', GrapesJSDetailView.as_view(), name='template'),
    re_path('^template/load/(?P<pk>\d+)/?$', GrapesJSLoadView.as_view(), name='template-load'),
    re_path('^template/save/(?P<pk>\d+)/?$', GrapesJSUpdateView.as_view(), name='template-save'),
    re_path('^template/delete/(?P<pk>\d+)/?$', GrapesJSDeleteView.as_view(), name='template-delete'),
    path("get_template/", GetTemplate.as_view(), name='dgjs_get_template'),

]
urlpatterns += i18n_patterns(
    path("", include("cms.urls")),
    url(r"^jsi18n/$", JavaScriptCatalog.as_view(), name="javascript-catalog"),
    prefix_default_language=False,
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
