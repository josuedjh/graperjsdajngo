# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib import admin
from django.db import models
from django.forms import Textarea

from .models import Snippet


@admin.register(Snippet)
class SnippetAdmin(admin.ModelAdmin):
	...
