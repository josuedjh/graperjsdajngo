# Generated by Django 2.2 on 2020-05-29 02:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('cms', '0022_auto_20180620_1551'),
    ]

    operations = [
        migrations.CreateModel(
            name='Snippet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, unique=True, verbose_name='Name')),
                ('html', models.TextField(blank=True, verbose_name='HTML')),
                ('template', models.CharField(blank=True, max_length=255, verbose_name='Template')),
                ('slug', models.SlugField(default='', max_length=255, unique=True, verbose_name='Slug')),
            ],
            options={
                'verbose_name': 'Snippet',
                'verbose_name_plural': 'Snippets',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='SnippetPtr',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='plugin_snippetptr', serialize=False, to='cms.CMSPlugin')),
                ('snippet', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='plugin.Snippet')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
