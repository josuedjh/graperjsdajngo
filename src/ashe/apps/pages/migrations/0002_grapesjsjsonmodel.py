# Generated by Django 2.2 on 2020-06-13 04:12

from django.db import migrations, models
import django_grapesjs.models.base


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='GrapesJSJSONModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, null=True)),
                ('domain', models.CharField(blank=True, max_length=255, null=True)),
                ('path', models.CharField(blank=True, max_length=255, null=True)),
                ('gjs_assets', models.TextField(blank=True, null=True)),
                ('gjs_css', models.TextField(blank=True, null=True)),
                ('gjs_html', models.TextField(blank=True, null=True)),
                ('gjs_styles', models.TextField(blank=True, null=True)),
                ('gjs_components', models.TextField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'grapes model',
                'verbose_name_plural': 'grapesjs models',
            },
            bases=(django_grapesjs.models.base.ModelToDictMixin, models.Model),
        ),
    ]
