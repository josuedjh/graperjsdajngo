CURRENT_DIR=$(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))

.PHONY: default
default: install

.PHONY: install
install:
	pip install -r requeriments.txt

.PHONY: runserver
runserver:
	python src/manage.py runserver 0.0.0.0:8000

.PHONY: backend
backend:
	docker-compose run --rm --service-ports --use-aliases backend --shell

.PHONY: makemessages
makemessages:
	python src/manage.py makemessages --all

.PHONY: compilemessages
compilemessages:
	python src/manage.py compilemessages